from django.db import models
from django import forms
from django.conf import settings


# Create your models here.

class Document(models.Model):
    docfile = models.ImageField(upload_to='images/')
    uploaded_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.pk)
