import urllib.request

from django.core.files.storage import FileSystemStorage
from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader, RequestContext
from django.urls import reverse
from .models import Document
from .forms import UploadFileForm
from django.views.generic import TemplateView
from django.http import JsonResponse
import cv2
import re
import numpy as np
import pytesseract


def homepage(request):
    home_template = loader.get_template('home.html')
    return HttpResponse(home_template.render())


class MainView(TemplateView):
    template_name = 'main.html'


def extract_data(input_list, information):
    return [x for x in input_list if f'{information}' in x]


def only_number(sequence):
    txt_list = []
    pattern = r'[0-9\,\.]+'
    for element in sequence:
        new_element = re.findall(pattern, element)
        txt_list.append(new_element)
    if len(txt_list) > 0:
        return ' '.join(map(str, txt_list))


def _grab_image(path=None, stream=None, url=None):
    if path is not None:
        facture = cv2.imread(path)

    else:
        if url is not None:
            resp = urllib.request.urlopen(url)
            data = resp.read()
            print("hehehe")
        elif stream is not None:
            data = stream.read()
            print("aaaa")

        facture = np.asarray(bytearray(data), dtype="uint8")
        facture = cv2.imdecode(facture, cv2.IMREAD_COLOR)
        print(facture)
    return facture

    # number = self.extract_data(split_list, "Facture")
    # hors_tax = self.extract_data(split_list, "HT")
    # tva = self.extract_data(split_list, "TVA")
    # all_price = self.extract_data(split_list, "TTC")
    # print(type(self.only_number(hors_tax)))
    # print(self.only_number(tva))
    # print(self.only_number(hors_tax))


def file_upload_view(request):
    # print(request.FILES)
    data = {"success": False}
    if request.method == "POST":
        my_file = request.FILES.get('file')
        if request.FILES.get('file', None) is not None:
            image = _grab_image(stream=request.FILES.get('file'))  ## grab uploaded image
            txt_list = pytesseract.image_to_string(image)
            split_list = txt_list.split('\n')
            print(txt_list)
            print('hahah')
            split_list[:] = [x for x in split_list if x]
            number = extract_data(split_list, "Facture")
            hors_tax = extract_data(split_list, "HT")
            tva = extract_data(split_list, "TVA")
            all_price = extract_data(split_list, "TTC")
            print(type(only_number(hors_tax)))
            print(only_number(tva))
            print(only_number(hors_tax))

            form = UploadFileForm(request.POST, request.FILES)
            my_file = request.FILES.get('file')
            facture = Document.objects.create(docfile=my_file)
            facture.save()
            print(str(facture.docfile))

        ### no data = raise error
        else:
            url = request.POST.get("url", None)
            if url is None:
                data['error'] = "No URL provided"
                return JsonResponse(data)
            ## load image and convert
            image = _grab_image(url=url)
            image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

        data["success"] = True
        return HttpResponse('The name of the file is' + str(facture.pk))
    else:
        form = UploadFileForm()

    return JsonResponse({'post': 'false'})

# def upload_file(request):
#     if request.method == "POST":
#         form = UploadFileForm(request.POST, request.FILES)
#         if form.is_valid():
#             new_document = Document(docfile=request.FILES['docfile'])
#             new_document.save()
#             return HttpResponse(reverse('detectorapp.views.upload_file'))
#     else:
#         form = UploadFileForm()
#     documents = Document.objects.all()
#     context = {'form': form, 'documents': documents}
#
#     return render(request, 'home.html', context)
