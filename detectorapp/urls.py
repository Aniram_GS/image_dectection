from django.contrib import admin
from django.urls import path
from . import views
from .views import MainView, file_upload_view

urlpatterns = [
    #path('', views.upload_file, name='home'),
    path('', MainView.as_view(), name="main_view"),
    path('upload/', file_upload_view, name="upload")


]